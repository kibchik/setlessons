"use strict";
var users = angular.module('users', ['ngDialog'])

users.controller('UserList', function($scope, $http, ngDialog) {

    $http.get('api/users.json').success(function(data, status, header, config) {
        $scope.users = data;
    });

    $scope.head = {
        'number': "#",
        'name': "Имя",
        'age': "Возраст",
        'phone': "Телефон",
        'email': "Email"
    };

    $scope.sort = {
        column: 'number',
        arrow: 'down',
        descending: false
    };

    $scope.usersEdit = {};

    $scope.selectedCls = function(column) {
        return column == $scope.sort.column && 'glyphicon-menu-' + $scope.sort.arrow;
    };

    $scope.changeSorting = function(column) {
        var sort = $scope.sort;
        if (sort.column == column) {
            if (sort.arrow == 'down') {
                sort.arrow = 'up';
            } else {
                sort.arrow = 'down';
            }
            sort.descending = !sort.descending;
        } else {
            sort.column = column;
            sort.arrow = 'down';
            sort.descending = false;
        }
    };

    $scope.clickToDelete = function(index) {
        var apiQuery = '';
        if (Math.random() >= 0.5) {
            apiQuery = '';
        } else {
            apiQuery = 'user_remove_success';
        }
        $http.get('api/' + apiQuery + '.json').success(function(data, status, header, config) {
            $scope.users.splice(index, 1);
            for (var i in data) alert(i + ' : ' + data[i]);
        }).error(function(value) {
            alert(value);
        });
    };

    $scope.clickToOpen = function(index) {
        $scope.rowIndex = index;
        $scope.openConfirm();
        for (var i in $scope.users[$scope.rowIndex]) {
            $scope.usersEdit[i] = $scope.users[$scope.rowIndex][i];
        };
        $scope.usersEditError = '';
    };

    $scope.openConfirm = function() {
        var dialog = ngDialog.open({
            template: 'template/usersEdit.html',
            scope: $scope
        });
        dialog.closePromise.then(function(data) {
            if (data.value) {
                var apiQuery = '';
                var closePromise = null;
                var closeDialog = null;

                if (Math.random() >= 0.5) {
                    apiQuery = 'user_update_error';
                } else {
                    apiQuery = 'user_update_success';
                }
                $http.get('api/' + apiQuery + '.json').success(function(data, status, header, config) {

                }).then(function(response) {
                    if (response.data.errors != undefined) {
                        if (Math.random() >= 0.5) {
                            $scope.usersEditError = response.data.errors[0];
                            console.log(response.data.errors[0]);
                        } else {
                            $scope.usersEditError = response.data.errors[1];
                            console.log(response.data.errors[1]);
                        }
                        $scope.openConfirm();
                    } else {
                        for (var i in $scope.users[$scope.rowIndex]) {
                            if ($scope.usersEdit[i] != $scope.users[$scope.rowIndex][i]) {
                                $scope.users[$scope.rowIndex][i] = $scope.usersEdit[i];
                            }
                        }
                    }
                });
            }
        });
    };

});
