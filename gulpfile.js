'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    minifyHTML = require('gulp-htmlmin'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sassFast = require('gulp-sass'),
    sassGood = require('gulp-ruby-sass'),
    sourcemaps = require('gulp-sourcemaps'),

    fileInclude = require('gulp-file-include'),
    cssmin = require('gulp-minify-css'),
    uncss = require('gulp-uncss'),

    rimraf = require('rimraf'),
    rename = require("gulp-rename"),
    browserSync = require("browser-sync"),
    reload = browserSync.reload,

    //start for browserify
    debowerify = require('debowerify'),
    browserifyShim = require('browserify-shim'),
    browserify = require('browserify'),
    watchify = require('watchify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    gutil = require('gulp-util'),
    assign = require('lodash.assign'),

    ngAnnotate = require('gulp-ng-annotate'),

    destFile = 'main.js';
var customOpts = {
    entries: ['src/js/main.js'],
    debug: true,
    noparse: ['jquery', 'jquery-mobile']
};
var opts = assign({}, watchify.args, customOpts);
var b = watchify(browserify(opts).transform(debowerify).transform('browserify-shim'));
//end for browserify

var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/',
        js: 'build/js/',
        // styleCkeditor: 'build/js/ckeditor/',
        css: 'build/css/',
        img: 'build/img/',
        fonts: 'build/fonts/',
        api: 'build/api/',
        templates: 'build/template/'
    },
    src: { //Пути откуда брать исходники
        html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        js: 'src/js/main.js', //В стилях и скриптах нам понадобятся только main файлы
        app: 'src/js/partials/app.js',
        style: 'src/style/main.scss',
        styleVendor: 'src/style/vendor/',
        img: 'src/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных
        fonts: 'src/fonts/**/*.*',
        api: 'src/api/**/*.*',
        templates: 'src/template/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        style: 'src/style/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        api: 'src/api/**/*.*',
        vendorjs: 'bower_components/**/*.js',
        vendorStyle: 'bower_components/**/*.css'
    },
    clean: './build'
};
var config = {
    server: {
        baseDir: "./build",
        index: ["index.html"]
    },
    reloadDelay: 2000,
    tunnel: false,
    host: 'marietti',
    port: 9000,
    logPrefix: "marietti"
};
gulp.task('copy:build', function() {
    gulp.src("src/*.*")
        .pipe(gulp.dest(path.build.html));
    gulp.src("src/.*")
        .pipe(gulp.dest(path.build.html));
    gulp.src(path.src.api)
        .pipe(gulp.dest(path.build.api));
    gulp.src(path.src.templates)
        .pipe(gulp.dest(path.build.templates));
    gulp.src("./bower_components/ng-dialog/css/ngDialog.css")
        .pipe(rename(function(path) {
            path.basename = "_" + path.basename,
                path.extname = ".scss"
        }))
        .pipe(gulp.dest(path.src.styleVendor));
    gulp.src("./bower_components/ng-dialog/css/ngDialog-theme-default.css")
        .pipe(rename(function(path) {
            path.basename = "_" + path.basename,
                path.extname = ".scss"
        }))
        .pipe(gulp.dest(path.src.styleVendor));
});
gulp.task('html:build', function() {
    gulp.src(path.src.templates)
        .pipe(fileInclude()).on('error', function(err) {
            console.error('Error', err.message);
            this.emit('end');
        })
        .pipe(minifyHTML({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest(path.build.templates));
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(fileInclude()).on('error', function(err) {
            console.error('Error', err.message);
            this.emit('end');
        })
        .pipe(minifyHTML({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
        .pipe(reload({
            stream: true
        })); //И перезагрузим наш сервер для обновлений
});
gulp.task('js:build', function() {
    gulp.src(path.src.app)
        .pipe(gulp.dest(path.build.js));
    return b.bundle().on('error', function(err) {
            console.error('Error', err.message);
            this.emit('end');
        })
        .pipe(source('main.js'))
        // optional, remove if you don't need to buffer file contents
        .pipe(buffer())
        // optional, remove if you dont want sourcemaps
        .pipe(sourcemaps.init({
            loadMaps: true
        })) // loads map from browserify file
        // Add transformation tasks to the pipeline here.
        .on('error', function(err) {
            console.error('Error', err.message);
            this.emit('end');
        })
        .pipe(sourcemaps.write('../maps')) // writes .map file
        .pipe(gulp.dest('build/js'))
        .pipe(reload({
            stream: true
        }));
});
gulp.task('js:prod', function() {
    gulp.src(path.src.app)
        .pipe(ngAnnotate({
            // true helps add where @ngInject is not used. It infers.
            // Doesn't work with resolve, so we must be explicit there
            add: true
        }))
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js));
    return b.bundle()
        .pipe(source('main.js'))
        // optional, remove if you don't need to buffer file contents
        .pipe(buffer())
        // Add transformation tasks to the pipeline here.
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(gulp.dest('build/js'))
        .pipe(reload({
            stream: true
        }));;
});
gulp.task('style:build', function() {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(sourcemaps.init())
        .pipe(sassFast({
            style: 'expanded'
        })).on('error', function(err) {
            console.error('Error', err.message);
        })
        .pipe(sourcemaps.write('../maps', {
            includeContent: false,
        }))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({
            stream: true
        }));
    // gulp.src(path.src.styleCkeditor) //Выберем наш main.scss
    // .pipe(sourcemaps.init())
    //     .pipe(sassFast({
    //         style: 'expanded'
    //     })).on('error', function(err) {
    //         console.error('Error', err.message);
    //     })
    //     .pipe(sourcemaps.write('../maps', {
    //         includeContent: false,
    //     }))
    //     .pipe(gulp.dest(path.build.styleCkeditor))

});
gulp.task('style:prod', function() {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(sassFast({
            style: 'expanded'
        }))
        .on('error', function(err) {
            console.error('Error', err.message);
        })
        .pipe(autoprefixer({
            browsers: ['> 3%', 'last 2 versions', 'Firefox < 20'],
            cascade: false
        }))
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({
            stream: true
        }));
});
gulp.task('uncss:prod', ['style:prod', 'html:build'], function() {
    gulp.src(path.build.css + 'main.css')
        .pipe(uncss({
            html: ['build/*.html'],
            timeout: 5000,
            ignore: [/\.chosen*/, /\.filestyle*/, /\.bootstrap*/, /\.dropdown/, /\.simple_*/, /dd*/, /simpleSlidePosition/,
                ".fade",
                ".fade.in",
                ".collapse",
                ".collapse.in",
                ".collapsing",
                /\.open/
            ]
        }))
        .pipe(cssmin()) //Сожмем
        .pipe(gulp.dest(path.build.css))
});
gulp.task('image:build', ['copy:build'], function() {
    gulp.src(path.src.img) //Выберем наши картинки
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({
            stream: true
        }))
});
gulp.task('image:prod', ['copy:build'], function() {
    gulp.src(path.src.img) //Выберем наши картинки
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({
            stream: true
        }))
});
gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});
gulp.task('build', [
    'copy:build',
    'html:build',
    'js:build',
    'style:build',
    'fonts:build',
    'image:build'
]);
gulp.task('buildProduction', [
    'copy:build',
    'html:build',
    'js:prod',
    'style:prod',
    // 'uncss:prod',
    'fonts:build',
    'image:prod'
]);
gulp.task('watch:build', function() {
    watch([path.watch.vendorStyle], function(event, cb) {
        gulp.start('copy:build');
    });
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});
gulp.task('watch:prod', function() {
    watch([path.watch.vendorStyle], function(event, cb) {
        gulp.start('copy:build');
    });
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:prod');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:prod');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:prod');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});
gulp.task('webserver', function() {
    browserSync(config);
});
gulp.task('clean', function(cb) {
    rimraf(path.clean, cb);
});
gulp.task('default', ['build', 'webserver', 'watch:build']);
gulp.task('prod', ['buildProduction', 'webserver', 'watch:prod']);
